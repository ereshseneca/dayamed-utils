import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created By Gorantla, Eresh on 22/Oct/2019
 **/

public class S3Upload {

	public static void main(String[] a) throws Exception {
		String accessKey = null;
		String secretKey = null;
		String bucketName = null;
		try (InputStream input = S3Upload.class.getClassLoader()
		                                       .getResourceAsStream("config.properties")) {
			Properties prop = new Properties();
			if (input != null) {
				prop.load(input);
				accessKey = prop.getProperty("s3.access.key");
				secretKey = prop.getProperty("s3.secret.key");
				bucketName = prop.getProperty("s3.bucket.name");
			}
		}
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		if (a.length >= 1) {
			String fileName = a[0];
			String filePath = a[1];
			AmazonS3 s3client = AmazonS3ClientBuilder
					.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials))
					.withRegion(Regions.US_EAST_2)
					.build();
			s3client.putObject(bucketName, fileName, new File(filePath));
		}
	}
}
