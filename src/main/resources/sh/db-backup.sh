#!/bin/bash
NOW=$(date +"%Y-%m-%d")

PROPERTY_FILE=config.properties


BACKUP_DIR=$(cat "${PROPERTY_FILE}" | grep "backup.dir" | cut -d'=' -f2)
echo "Backup ==> $BACKUP_DIR"
SOURCE_DIR=$(cat ${PROPERTY_FILE} | grep "source.dir" | cut -d'=' -f2)
JAR_FILE_NAME=$(cat ${PROPERTY_FILE} | grep "application.jar.file.name" | cut -d'=' -f2)

MYSQL_HOST=$(cat ${PROPERTY_FILE} | grep "database.host.name" | cut -d'=' -f2)

MYSQL_PORT=$(cat $PROPERTY_FILE | grep "database.port" | cut -d'=' -f2)
MYSQL_USER=$(cat $PROPERTY_FILE | grep "database.user.name" | cut -d'=' -f2)
MYSQL_PASSWORD=$(cat $PROPERTY_FILE | grep "database.password" | cut -d'=' -f2)
DATABASE_NAME=$(cat $PROPERTY_FILE | grep "database.schema.name" | cut -d'=' -f2)

mkdir -p $BACKUP_DIR

backup_mysql(){
         mysqldump -h $MYSQL_HOST \
           -P $MYSQL_PORT \
           -u $MYSQL_USER \
           -p$MYSQL_PASSWORD $DATABASE_NAME | gzip > $BACKUP_DIR/$DATABASE_NAME-${NOW}.sql.gz
}

backup_mysql

JAVA_HOME=/usr/bin/java
java -jar $SOURCE_DIR/$JAR_FILE_NAME $DATABASE_NAME-${NOW}.sql.gz $BACKUP_DIR/$DATABASE_NAME-${NOW}.sql.gz
rm $BACKUP_DIR/$DATABASE_NAME-${NOW}.sql.gz
